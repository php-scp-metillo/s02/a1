<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02 Activity</title>
</head>
<body>
	<!-- ACTIVITY 01 -->
	<h2>Divisibles of Five</h2>
	<?php getDivisibleByFive(); ?>

	<!-- ACTIVITY 02 -->
	<h2>Array Manipulation</h2>
	<?php $students = []; ?>

	<?php array_push($students, "John Smith"); ?>
	<p><?php var_dump($students); ?></p>
	<p><?php echo count($students); ?></p>

	<?php array_push($students, "Jane Smith"); ?>
	<p><?php var_dump($students); ?></p>
	<p><?php echo count($students); ?></p>	

	<?php array_shift($students); ?>
	<p><?php var_dump($students); ?></p>
	<p><?php echo count($students); ?></p>	

</body>
</html>